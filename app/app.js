'use strict';
var baseUrl="http://localhost:4000";
var defaultHeaders={}
// Declare app level module which depends on views, and core components
var COLORS = ['#787a7c', '#eed99e','#8deeb7'];
angular.module('myApp', [
  'ngRoute',
  'ngMaterial',
  'ngMessages'
]).config(function($routeProvider) {
  $routeProvider
      .when("/",{
        templateUrl: "PostList.html"
      })
      .when("/posts",{
        templateUrl: "PostList.html"
      })
      .when("/posts/:id", {
        templateUrl : "PostView.html"
      })/*
      .when("/login",{
        templateUrl:"Login.html"
      })*/
}).factory(
    'PostFactory', function ($q, $http){
      return{
        CommentPost: (comment)=>{
          let defer = $q.defer();
          let req = {
            method: 'POST',
            url: baseUrl + '/posts/' + comment.postId+ "/comments",
            headers: defaultHeaders,
            data: comment
          }
          $http(req).then((response) => {
            console.log(response)
            defer.resolve(response.data)
          }, (err) => {
            defer.resolve(null);
          });
          return defer.promise
        },
        UpdatePost: (post)=> {
          let defer = $q.defer();
          let req = {
            method: 'PATCH',
            url: baseUrl + '/posts/' + post._id,
            headers: defaultHeaders,
            data: post
          }
          $http(req).then((response) => {
            console.log(response)
            defer.resolve(response.data)
          }, (err) => {
            defer.resolve(null);
          });
          return defer.promise
        },
        GetPostById: (id) =>{
          let defer = $q.defer();
          let req = {
            method: 'GET',
            url: baseUrl+'/posts/'+id,
            headers: {
              'Content-Type': 'application/json',
            },
            data: {}
          }
          $http(req).
          then((response) =>{
           defer.resolve(response.data)
          },(err)=>{
            defer.resolve(null);
          });
            return defer.promise
              },
        GetPosts: () =>{
          let defer = $q.defer();
          let req = {
            method: 'GET',
            url: baseUrl+'/posts',
            headers: {
              'Content-Type': 'application/json',
            },
            data: {}
          }
          $http(req).
          then(function(response) {

            let data = response.data.map((elem) => {
              return {
                url: encodeURI('/posts/' + elem._id),
                subtitle: elem.subtitle,
                title: elem.title,
                image: elem.image,
                color: COLORS[Math.floor(Math.random() * COLORS.length)]
              }
            })
            defer.resolve(data);
          },(err)=>{
            console.log(err);
            defer.resolve(null);
          })
          return defer.promise
        }
      }
    })
    .controller('postListCtrl', function($scope,$http,$q,PostFactory) {
      $scope.tiles=[]
      $scope.getTiles = () => {
        $q.when(PostFactory.GetPosts()).then(function (result) {

          $scope.tiles = result;
          console.log($scope.tiles)
        });
      }
      $scope.onTileClick= (tile)=>{
        //console.log(tile.url)
        window.location.href = tile.url;
      }
      $scope.getTiles()
      });

/*.
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/view1'});
}]);*/
